Debug = {

  init: function() {
    this.d = Utils.$('#debug-code');
    this.push('Chargement OK!');
    this.d.addEventListener('click',Debug.reset);
  },

  push: function(str, error) {
    if (error) {
      this.d.innerHTML = '<span class=\'error\'>'+str+'</span>'+"\r\n" + this.d.innerHTML;
    } else {
      this.d.innerHTML = (str + "\r\n") + this.d.innerHTML;
    }
  },

  reset: function(evt) {
    Debug.d.innerHTML = '';
  }
}