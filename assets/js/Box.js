Box = {

  init: function() {

    this.box = {
      box:Utils.$('#box'),
      title:Utils.$('#boxtitle'),
      button1:Utils.$('#boxb1'),
      button2:Utils.$('#boxb2'),
      txt:Utils.$('#boxtxt'),
      voile:Utils.$('#boxvoile')
    };
    this.box.voile.addEventListener('click', Box.shutdown);
  },

  set: function(title, txt, button1, button2) {
    this.box.title.innerHTML  = title;
    this.box.txt.innerHTML    = txt;
    this.box.button1.innerHTML = button1.txt;
    this.box.button2.innerHTML = button2.txt;
    this.box.button1.addEventListener('click', button1.callback);
    this.box.button2.addEventListener('click', button2.callback);
    this.box.box.className = 'show';
  },

  reset: function() {
    Box.box.title.innerHTML = Box.box.button1.innerHTML = Box.box.button2.innerHTML = Box.box.txt.innerHTML = "";
    var b1 = document.createElement('button');
    b1.id  = "boxb1";
    var b2 = document.createElement('button');
    b2.id  = "boxb2";
    Box.box.button1.parentNode.replaceChild(b1, Box.box.button1);
    Box.box.button2.parentNode.replaceChild(b2, Box.box.button2);
    this.box = {
      box:Utils.$('#box'),
      title:Utils.$('#boxtitle'),
      button1:Utils.$('#boxb1'),
      button2:Utils.$('#boxb2'),
      txt:Utils.$('#boxtxt'),
      voile:Utils.$('#boxvoile')
    };
  },

  shutdown: function() {
    Box.box.box.className = 'hidden';
    Box.reset();
  }
}