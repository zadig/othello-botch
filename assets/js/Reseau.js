Reseau = {

  init: function() {
    this.input  = Utils.$('#idPartie');
    this.creerB = Utils.$('#reseau-create');

    Utils.$('#reseau-form').addEventListener('submit', function(e) {
      Game.reset();
      e.preventDefault();
      e.stopPropagation();
      var idPartie  = this.input.value.trim();
      if (idPartie == '') {
        Debug.push('Veuillez entrer l\'id de la partie.', true);
        this.input.value = '';
        this.input.focus();
        return false;
      }
      this.idPartie = idPartie;
      Game.new(5);
      Debug.push('Connexion à la partie '+idPartie+' en cours...');
      return false;
    }.bind(this));

    this.creerB.addEventListener('click', function() {
      Game.reset();
      Game.new(6);
      Debug.push('Création d\'une partie en cours...');
    }.bind(this));

  },

  msg: function(data) {
    if (data.method == 'requestIdPartie') {
      Connector.ws.send(JSON.stringify({idPartie:parseInt(this.idPartie), action: 'join'}));
    } else if (data.method == 'create') {
      var idPartie  = parseInt(data.idPartie);
      this.input.value = idPartie;
      Box.set("Défiez votre adversaire !","Tu joues les Noirs (<span class='noirs'></span>).<br>Voici l'identifiant de ta partie : <input type='text' value='"+Utils.$('#idPartie').value+"'>.",{txt:'Ok!',callback:Box.shutdown},{txt:'Annuler',callback:function() { Box.shutdown(); Game.reset(); }});
      Debug.push('Partie créée. Vous pouvez commencer à jouer.<br>(L\'adversaire n\'est peut-être toujours pas en ligne...');
    } else if (data.method == 'join') {
      Debug.push('Partie rejointe. En attente de l\'adversaire.');
    }
  }
}
