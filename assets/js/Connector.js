Connector = {
  config: {
    ws: "ws://"+document.location.hostname+":8881"
  },

  newGame: function(mode, Game) {
    this.reset();
    this.Game = Game;
    this.ws   = new WebSocket(this.config.ws);
    this.ws.addEventListener('open',this.onOpen);
    this.ws.addEventListener('open',function() {
      if (parseInt(mode) < 5) {
        this.ws.send(JSON.stringify({action:'connect',mode:mode}));
      } else {
        this.ws.send(JSON.stringify({action:'connect',mode:mode, ia:Utils.$('#reseau-ia').checked}));
      }
    }.bind(this));
    this.ws.addEventListener('close',this.onClose);
    this.ws.addEventListener('error',this.onError);
    this.ws.addEventListener('message',this.onMsg);
  },

  onOpen: function(evt) {
    Debug.push('WS: OK');
  },

  onError: function(evt) {
    Debug.push('WS erreur.');
    console.log(evt);
  },

  onClose: function(evt) {
    Debug.push('WS fermé.');
    if (Utils.$('#loop').checked) {
      Game.reset();
      Game.new(4);
    } else {
      console.log('no loop');
    }
  },

  onMsg: function(evt) {
    Game.msg(JSON.parse(evt.data));
  },

  reset: function() {
    try {
      this.ws.close();
    } catch(e) {}
    this.ws = null;
    this.Game = null;
  }
}