Stats = {

  init: function() {
    this.score = {b:Utils.$('#score-b'),
                  w:Utils.$('#score-w'),
                  free: Utils.$('#score-f'),
                  'free can': Utils.$('#score-f')};
    this.calculate();
  },

  calculate: function() {
    Stats.score['b'].innerHTML = 0;
    Stats.score['w'].innerHTML = 0;
    for(var l=0;l<10;l++) {
      for (var c=0;c<10;c++) {
        var classe = Utils.$('#o-'+l+''+c).className;
        Stats.score[classe].innerHTML = parseInt(Stats.score[classe].innerHTML)+1;
      }
    }
    pb = 100*parseInt(Stats.score['b'].innerHTML)/(parseInt(Stats.score['b'].innerHTML)+parseInt(Stats.score['w'].innerHTML))
    this.score.b.parentNode.style.width = pb+'%';
    this.score.w.parentNode.style.width = (100-pb)+'%';
  },

  reset: function() {
    this.score.b.className = this.score.w.className = '';
  }
}
