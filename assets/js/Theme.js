Theme = {

  themes: [
    {
      name: 'Deadmau5',
      html:'deadmau5',
      mp3:'/assets/mp3/deadmau5.webm',
      video:'/assets/video/deadmau5.mp4'
    },

    {
      name: 'Star Wars',
      html:'starwars',
      mp3:'/assets/mp3/starwars.mp3',
      video:'https://static.baboom.com/d/prod/src/Content/Splash/assets/videos/b_bang_splash.mp4?ilee6915'
    },

    {
      name: 'Apple vs FBI',
      html: 'apple',
      mp3: '/assets/mp3/apple.webm',
      video:'/assets/video/apple.webm'
    },

    {
      name: 'Fsociety',
      html: 'fsociety',
      mp3: '/assets/mp3/fsociety.webm',
      video: '/assets/video/fsociety.mp4'
    }

  ],

  init: function() {
    this.getDefault();
    this.select  = Utils.$('#theme');
    for(var i = 0;i < this.themes.length;i++) {
      var t       = this.themes[i];
      var o       = document.createElement('option');
      o.value     = i;
      o.innerHTML = t.name;
      if (i == this.current) {
        o.selected = true;
      }
      this.select.appendChild(o);
    }
    this.select.addEventListener('input', Theme.change);
    Utils.$('#changetheme').addEventListener('click', Theme.change);
  },

  change: function() {
    var value = Theme.select.value;
    if (value == Theme.current) {
      return false;
    }
    if (Theme.themes[value] == null) {
      return false;
    }
    Utils.$('html').className = Utils.$('html').className.replace(Theme.themes[Theme.current].html, Theme.themes[value].html);
    Theme.current = value;
    Utils.$('#audio').src     = Theme.themes[Theme.current].mp3;
    Utils.$('#video').src     = Theme.themes[Theme.current].video;
    Utils.$('#video').play();
  },

  getDefault: function() {
    var c = Utils.$('html').className;
    for(var i = 0;i < this.themes.length;i++) {
      var t       = this.themes[i];
      if (t.html == c) {
        this.current = i;
        return null;
      }
    }
    Utils.$('html').className = this.themes[0].html;
    this.current              = 0;
  }
}