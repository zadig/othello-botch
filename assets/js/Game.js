Game = {

  init: function() {

    // INIT QUADRILLAGE
    for (var l=0;l<10;l++) {
      for (var c=0;c<10;c++) {
        Utils.$('#o-'+l+''+c).addEventListener('click', function(el) {
          this.playOn(el.target);
        }.bind(this));
        Utils.$('#o-'+l+''+c).title = l+','+c;
      }
    }

    // INIT BOUTON RESET
    Utils.$('#reset').addEventListener('click', function() {
      this.reset();
    }.bind(this));
    this.reset();

    // INIT GAME MODE
    Utils.$('#launch').addEventListener('click', function() {
      this.reset();
      var mode = Utils.$('#mode').value;
      this.new(parseInt(mode));
    }.bind(this));
  },

  playOn: function(el) {
    if (!this.on) {
      return false;
    }
    var lc = this.parseLC(el.id);
    var l  = parseInt(lc[0]);
    var c  = parseInt(lc[1]);

    // Check if placement OK
    var ok = false;
    for (var i=0;i<this.can.length;i++) {
      var ca = this.can[i];
      if (ca[0] == l && ca[1] == c) {
        ok = true;
        break;
      }
    }
    if (!ok) {
      console.log('Placement non autorisé.');
      Debug.push('Placement non autorisé.',true);
      return false;
    }

    console.log('Placement autorisé.');
    this.sendToServer(l, c);
    this.setCan([])
    Debug.push('Placement en '+l+','+c+': OK');
  },

  sendToServer: function(l, c) {
    var data = JSON.stringify({action:'play', 'l':l,'c':c});
    Connector.ws.send(data);
  },

  parseLC: function(id) {
    m = id.match(/^o-([0-9])([0-9])$/);
    return [m[1], m[2]];
  },

  reset: function() {
    for (var l=0;l<10;l++) {
      for (var c=0;c<10;c++) {
        Utils.$('#o-'+l+''+c).className = 'free';
      }
    }
    Utils.$('#o-45').className = Utils.$('#o-54').className = 'b';
    Utils.$('#o-44').className = Utils.$('#o-55').className = 'w';
    Stats.calculate();
    Connector.reset();
    this.current = 'b';
    this.on      = false;
    Utils.$('#video').style.display = 'none';
    Utils.$('html').className       = Utils.$('html').className.replace(' hide', '');
    document.body.className         = '';
    Stats.reset();
  },

  new: function(mode) {
    this.reset();
    this.on = true;
    this.mode = mode;
    Connector.newGame(mode, this);
    switch (parseInt(mode)) {
      case 2:
        Box.set("Tu ne peux pas gagner contre Cerebro !","Tu joues les Noirs (<span class='noirs'></span>).<br>Prépare-toi à perdre !",{txt:'Ok!',callback:Box.shutdown},{txt:'Annuler',callback:function() { Box.shutdown(); Game.reset(); }});
        break;

      case 3:
        Box.set("Cerebro vaincra toujours.","Tu joues les Blancs (<span class='blancs'></span>).<br>Prépare-toi à perdre !",{txt:'Ok!',callback:Box.shutdown},{txt:'Annuler',callback:function() { Box.shutdown(); Game.reset(); }});
        break;

      case 4:
        Box.set("Combat de Titan !","Admire un épique combat entre Cerebro et Cerebro...<br>Nous connaissons à l'avance le perdant..",{txt:'Ok!',callback:Box.shutdown},{txt:'Annuler',callback:function() { Box.shutdown(); Game.reset(); }});
        break;

      case 5:
        Box.set("Rejoignez votre adversaire !","Tu joues les Blancs (<span class='blancs'></span>).<br>Bats-toi !",{txt:'Ok!',callback:Box.shutdown},{txt:'Annuler',callback:function() { Box.shutdown(); Game.reset(); }});
        break;
    }
    Utils.$('#video').style.display = 'block';
    Utils.$('html').className       += ' hide';
    this.can = [];
  },

  msg: function(data) {
    if (data.error) {
      Debug.push('Game error:'+data.errstr,true);
      return false;
    }

    switch (data.action) {

      case 'play':
        this.current            = data.current;
        document.body.className = this.current;
        this.can     = data.can;
        this.setCan(data.can);
        break;

      case 'refresh':
        if (Utils.$('.lastMove') != null) {
          Utils.$('.lastMove').className = Utils.$('.lastMove').className.replace(' lastMove','');
        }
        var values = data.values;
        var last   = data.last;
        for (var i=0;i<values.length;i++) {
          var v = values[i];
          Utils.$('#o-'+v[0]+''+v[1]).className = v[2];
        }
        Stats.calculate();
        Utils.$('#o-'+last[0]+''+last[1]).className += ' lastMove';
        break;

      case 'end':
        var results = data.results;
        var nb      = results.b;
        var nw      = results.w;
        if (nb == nw) {
          Box.set('Un Dieu vs un autre Dieu...','Oh oh... Il semblerait que ce soit un match nul.<br>On remet ça ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
        } else if (nb > nw) {
          switch (parseInt(Game.mode)) {

            case 1:
              Box.set('La victoire revient aux Noirs !','Bravo, les noirs (<span class="noirs"></span>) on gagné !<br>Une revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 2:
              Box.set('Vous avez vaincu Cerebro...','Bravo, vous avez fait plier Cerebro. <br>Il veut sa revanche !',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 3:
              Box.set('Cerebro : vainqueur éternel!','C\'est dommage... Cerebro a encore gagné !. <br>Vous souhaitez une revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 4:
              Box.set('Cerebro a battu Cerebro !','Bravo, les noirs (<span class="noirs"></span>) on gagné !<br>On s\'en refait une ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 5:
              Box.set('L\'adversaire a gagné','Les gagnants sont les noirs (<span class="noirs"></span>)...<br>La revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 6:
              Box.set('Vous avez foutu le seum à l\'adversaire','Les gagnants sont les noirs (<span class="noirs"></span>) ... et c\'est vous ! <br>Il veut la revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;
          }

          Utils.$('#score-b').className = 'winner';
          Utils.$('#score-w').className = 'looser';
        } else {
          switch (parseInt(Game.mode)) {

            case 1:
              Box.set('La victoire revient aux Blancs !','Bravo, les blancs (<span class="blancs"></span>) on gagné !<br>Une revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 2:
              Box.set('Cerebro : vainqueur éternel!','C\'est dommage... Cerebro a encore gagné !. <br>Vous souhaitez une revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 3:
              Box.set('Vous avez vaincu Cerebro...','Bravo, vous avez fait plier Cerebro. <br>Il veut sa revanche !',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 4:
              Box.set('Cerebro a battu Cerebro !','Bravo, les blancs (<span class="blancs"></span>) on gagné !<br>On s\'en refait une ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 5:
              Box.set('Vous avez foutu le seum à l\'adversaire','Les gagnants sont les blancs (<span class="blancs"></span>) ... et c\'est vous ! <br>Il veut la revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;

            case 6:
              Box.set('L\'adversaire a gagné','Les gagnants sont les blancs (<span class="blancs"></span>)...<br>La revanche ?',{txt:'C\'est parti!',callback: function() { Game.new(Game.mode); Box.shutdown(); }},{txt:'Je suis peureux...',callback: Box.shutdown});
              break;
          }
          Utils.$('#score-b').className = 'looser';
          Utils.$('#score-w').className = 'winner';
        }
        break;

      case 'debug':
        var msg = data.msg;
        Debug.push(msg);
        break;

      case 'reseau':
        Reseau.msg(data);
        break;

    }
  },

  setCan: function(can) {
    var els = Utils.$$('.can');
    for (var i=0;i<els.length;i++) {
      els[i].className =els[i].className.replace(' can','');
    }
    for (var i=0;i<can.length;i++) {
      var c = can[i];
      Utils.$("#o-"+c[0]+''+c[1]).className = 'free can';
    }
  }
}